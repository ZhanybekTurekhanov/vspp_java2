<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Developer Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Developer
</h1>

<c:url var="addAction" value="/createdev/add" ></c:url>

<form:errors path="developer.*"/>

<form:form action="${addAction}" modelAttribute="developer">
<table>
	<c:if test="${!empty developer.name}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="name">
				<spring:message text="Name"/>
			</form:label>
		</td>
		<td>
			<form:input path="name" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="position">
				<spring:message text="Position"/>
			</form:label>
		</td>
		<td>
			<form:input path="position" />
		</td>
	</tr>
	<tr>
		<td>
			<form:label path="age">
				<spring:message text="Age"/>
			</form:label>
		</td>
		<td>
			<form:input path="age" />
		</td>
	</tr>
<!-- 	<tr> -->
<!-- 		<td>Branches:</td> -->
<%--                 <td><form:checkboxes items="${branches}" path="branch" --%>
<%--                        itemValue="branchId" itemLabel="branchName" /></td>  --%>
		
<!-- 	</tr> -->
	<tr>
		<td colspan="2">
			<c:if test="${!empty developer.name}">
				<input type="submit"
					value="<spring:message text="Edit Person"/>" />
			</c:if>
			<c:if test="${empty developer.name}">
				<input type="submit"
					value="<spring:message text="Add Person"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<%-- <form:select path="teamName"  multiple="true" items="${devTeams}" modelAttribute="devTeams"> --%>
<%-- 				<spring:message text="Team"/> --%>
<%-- 			</form:select> --%>
<c:if test="${!empty teams}">
	<table class="tg">
	<tr>
		<th width="120">DevTeam Name</th>
	</tr>
	<c:forEach items="${teams}" var="devTeam">
		<tr>
			<td>${devTeam.teamName}</td>

		</tr>
	</c:forEach>
	</table>
</c:if>
<br>
<h3>Devs List</h3>
<c:if test="${!empty listDevs}">
	<table class="tg">
	<tr>

		<th width="120">Dev Name</th>
		<th width="120">Dev position</th>
		<th width="120">Dev age</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listDevs}" var="dev">
		<tr>

			<td>${dev.name}</td>
			<td>${dev.position}</td>
			<td>${dev.age}</td>
			<td><a href="<c:url value='/edit/${dev.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/remove/${dev.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
<p><a href="<c:url value='/createteam' />">Developer teams</a> </p>
<p><a href="<c:url value='/createproject' />">Projects</a> </p>
</body>
</html>
