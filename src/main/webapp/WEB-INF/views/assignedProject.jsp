<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Assigned Team</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Assigned Team for project ${project.projectName}
</h1>
<h3>Project</h3>
<c:if test="${!empty devTeam}">
	<table class="tg">
	<tr>
		<th width="120">Project name</th>
		<th width="120">Project code</th>
		<th width="120">Field</th>
	</tr>
	
		<tr>
			<td>${project.projectName}</td>
			<td>${project.projectCode}</td>
			<td>${project.field}</td>

		</tr>
	
	</table>
</c:if>
<br>
<h3>Assigned Team</h3>
<c:if test="${!empty devTeam}">
	<table class="tg">
	<tr>
		<th width="120">DevTeam Name</th>
		<th width="120">DevTeam code</th>
		<th width="120">DevTeam Spec</th>
	</tr>
	
		<tr>
			<td>${devTeam.teamName}</td>
			<td>${devTeam.teamCode}</td>
			<td>${devTeam.spec}</td>

		</tr>
	
	</table>
</c:if>
<br>
<p><a href="<c:url value='/createproject' />">Back</a> </p>


</body>
</html>