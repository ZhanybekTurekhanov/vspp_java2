<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Project Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Create a Project
</h1>

<c:url var="addAction" value="/createproject/add" ></c:url>

<form:errors path="project.*"/>

<form:form action="${addAction}" modelAttribute="project">
<table>
	<c:if test="${!empty project.projectName}">
	<tr>
		<td>
			<form:label path="teamId">
				<spring:message text="Team ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="teamId" readonly="true" size="8"  disabled="true" />
			<form:hidden path="teamId" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="projectName">
				<spring:message text="Project name"/>
			</form:label>
		</td>
		<td>
			<form:input path="projectName" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="projectCode">
				<spring:message text="Project code"/>
			</form:label>
		</td>
		<td>
			<form:input path="projectCode" />
		</td>
	</tr>
	<tr>
		<td>
			<form:label path="field">
				<spring:message text="Field"/>
			</form:label>
		</td>
		<td>
			<form:input path="field" />
		</td>
	</tr>
	<tr>
		<td>Teams:</td>
                <td><form:select items="${listDevTeams}" path="devTeam"
                       itemValue="teamId" itemLabel="teamName" /></td> 
		
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty project.projectName}">
				<input type="submit"
					value="<spring:message text="Edit Project"/>" />
			</c:if>
			<c:if test="${empty project.projectName}">
				<input type="submit"
					value="<spring:message text="Add Project"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Projects List</h3>
<c:if test="${!empty projects}">
	<table class="tg">
	<tr>
		<th width="120">Project Name</th>
		<th width="120">Project Code</th>
		<th width="120">Field</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
		<th width="60">Project</th>
	</tr>
	<c:forEach items="${projects}" var="project">
		<tr>
			<td>${project.projectName}</td>
			<td>${project.projectCode}</td>
			<td>${project.field}</td>
			<td><a href="<c:url value='/createproject/edit/${project.teamId}' />" >Edit</a></td>
			<td><a href="<c:url value='/createproject/remove/${project.teamId}' />" >Delete</a></td>
			<td><a href="<c:url value='/createproject/project/${project.teamId}' />" >Project</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
<p><a href="<c:url value='/createteam' />">Developer teams</a> </p>
<p><a href="<c:url value='/createdev' />">Developers</a> </p>

</body>
</html>