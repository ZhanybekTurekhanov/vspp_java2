<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Dev Team Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Team
</h1>

<c:url var="addAction" value="/createteam/add" ></c:url>

<form:errors path="devTeam.*"></form:errors>

<form:form action="${addAction}" modelAttribute="devTeam">
<table>
	<c:if test="${!empty devTeam.teamName}">
	<tr>
		<td>
			<form:label path="teamId">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="teamId" readonly="true" size="8"  disabled="true" />
			<form:hidden path="teamId" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="teamName">
				<spring:message text="Name"/>
			</form:label>
		</td>
		<td>
			<form:input path="teamName" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="teamCode">
				<spring:message text="Code"/>
			</form:label>
		</td>
		<td>
			<form:input path="teamCode" />
		</td>
	</tr>
	<tr>
		<td>
			<form:label path="spec">
				<spring:message text="Spec"/>
			</form:label>
		</td>
		<td>
			<form:input path="spec" />
		</td>
	</tr>
	<tr>
		<td>Devs:</td>
                <td><form:checkboxes  items="${developers}" path="devs"
						itemValue="id" itemLabel="name" /></td> 
		
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty devTeam.teamName}">
				<input type="submit"
					value="<spring:message text="Edit Team"/>" />
			</c:if>
			<c:if test="${empty devTeam.teamName}">
				<input type="submit"
					value="<spring:message text="Add Team"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Teams List</h3>
<c:if test="${!empty listDevTeams}">
	<table class="tg">
	<tr>
		<th width="80">Dev ID</th>
		<th width="120">Dev Name</th>
		<th width="120">Dev code</th>
		<th width="120">Dev age</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
		<th width="60">Assign</th>
	</tr>
	<c:forEach items="${listDevTeams}" var="team">
		<tr>
			<td>${team.teamId}</td>
			<td>${team.teamName}</td>
			<td>${team.teamCode}</td>
			<td>${team.spec}</td>
			<td><a href="<c:url value='/createteam/edit/${team.teamId}' />" >Edit</a></td>
			<td><a href="<c:url value='/createteam/remove/${team.teamId}' />" >Delete</a></td>
			<td><a href="<c:url value='/createteam/assigned/${team.teamId}' />" >Assigned</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
<p><a href="<c:url value='/createdev' />">Developers</a> </p>
<p><a href="<c:url value='/createproject' />">Projects</a> </p>

</body>
</html>