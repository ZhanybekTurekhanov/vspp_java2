<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Branch Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Branch
</h1>

<c:url var="addAction" value="/createbranch/add" ></c:url>

<form:form action="${addAction}" modelAttribute="branch">
<table>
	<c:if test="${!empty branch.branchName}">
	<tr>
		<td>
			<form:label path="branchId">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="branchId" readonly="true" size="8"  disabled="true" />
			<form:hidden path="branchId" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="branchName">
				<spring:message text="Name"/>
			</form:label>
		</td>
		<td>
			<form:input path="branchName" />
		</td> 
	</tr>
	
	<tr>
<!-- 		<td>Branches:</td> -->
<%--                 <td><form:checkboxes items="${branches}" path="branch" --%>
<%--                         itemValue="branchId" itemLabel="branchName" /></td> --%>
		
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty branch.branchName}">
				<input type="submit"
					value="<spring:message text="Edit Branch"/>" />
			</c:if>
			<c:if test="${empty branch.branchName}">
				<input type="submit"
					value="<spring:message text="Add Branch"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<%-- <form:select path="teamName"  multiple="true" items="${devTeams}" modelAttribute="devTeams"> --%>
<%-- 				<spring:message text="Team"/> --%>
<%-- 			</form:select> --%>

<br>
<h3>Branch List</h3>
<c:if test="${!empty listBranches}">
	<table class="tg">
	<tr>
		<th width="80">Branch ID</th>
		<th width="120">Branch Name</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listBranches}" var="branche">
		<tr>
			<td>${branche.branchId}</td>
			<td>${branche.branchName}</td>
			<td><a href="<c:url value='/createbranch/edit/${dev.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/createbranch/remove/${dev.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>

</body>
</html>
