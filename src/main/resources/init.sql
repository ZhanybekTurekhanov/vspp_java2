DROP TABLE IF EXISTS `vspp`.`dev_team`;
CREATE TABLE `vspp`.`dev_team` (
  `TEAM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEAM_CODE` varchar(10) NOT NULL,
  `TEAM_NAME` varchar(20) NOT NULL,
  `SPEC` varchar(20) NOT NULL,
  PRIMARY KEY (`TEAM_ID`) USING BTREE,
  UNIQUE KEY `UNI_TEAM_NAME` (`TEAM_NAME`),
  UNIQUE KEY `UNI_TEAM_ID` (`TEAM_CODE`)
  );

DROP TABLE IF EXISTS `vspp`.`developer`;
CREATE TABLE `vspp`.`developer` (
  `DEV_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(30) NOT NULL,
  `AGE` int(2) NOT NULL,
  `POSITION` varchar(255) NOT NULL,
  `FK_BRANCH` int(10) NOT NULL,
  PRIMARY KEY (`DEV_ID`) 
);

DROP TABLE IF EXISTS `vspp`.`developer_dev_team`;
CREATE TABLE  `vspp`.`developer_dev_team` (
  `TEAM_ID` int(10) unsigned NOT NULL,
  `DEV_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`TEAM_ID`,`DEV_ID`),
  CONSTRAINT `FK_DEVELOPER_ID` FOREIGN KEY (`DEV_ID`) REFERENCES `developer` (`DEV_ID`),
  CONSTRAINT `FK_DEV_TEAM_ID` FOREIGN KEY (`TEAM_ID`) REFERENCES `dev_team` (`TEAM_ID`)
);

DROP TABLE IF EXISTS `vspp`.`project`;
CREATE TABLE  `vspp`.`project` (
  `FK_DEVTEAM` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROJECT_CODE` varchar(10) NOT NULL,
  `PROJECT_NAME` varchar(20) NOT NULL,
  `FIELD` varchar(20) NOT NULL,
  PRIMARY KEY (`FK_DEVTEAM`) USING BTREE,
  CONSTRAINT `FK_TEAM_ID` FOREIGN KEY (`FK_DEVTEAM`) REFERENCES `dev_team` (`TEAM_ID`)
);
















