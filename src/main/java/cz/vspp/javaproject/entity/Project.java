package cz.vspp.javaproject.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="project", catalog = "vspp")
public class Project {

	private Integer teamId;
	private String projectName;
	private String projectCode;
	private String field;
	private DevTeam devTeam;
	
	
	public Project() {
		
	}


	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "devTeam"))
	@Column(name="FK_DEVTEAM")
	public Integer getTeamId() {
		return teamId;
	}


	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}


	@Column(name="PROJECT_NAME", nullable = false) 
	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	@Column(name="PROJECT_CODE", unique = true, nullable = false)
	public String getProjectCode() {
		return projectCode;
	}


	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	
	@Column(name="FIELD", nullable = false)
	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	public DevTeam getDevTeam() {
		return devTeam;
	}


	public void setDevTeam(DevTeam devTeam) {
		this.devTeam = devTeam;
	}
	
	
	
}
