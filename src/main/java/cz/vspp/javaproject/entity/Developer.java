package cz.vspp.javaproject.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.util.AutoPopulatingList;

@Entity
@Table(name = "developer")
public class Developer implements Serializable{

	
	private Integer id;
	private String name;
	private Integer age;
	private String position;
	private List<DevTeam> teams = new AutoPopulatingList<>(DevTeam.class);
//	private Branch branch;
	
	
	public Developer() {
		
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DEV_ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable=false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	@Column(name="AGE", nullable = false)
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "POSITION", nullable = false)
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "devs")
	public List<DevTeam> getTeams() {
		return teams;
	}

	public void setTeams(List<DevTeam> teams) {
		this.teams = teams;
	}
	
//	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
//	@JoinTable(name = "developer_branch", joinColumns = { 
//	@JoinColumn(name = "DEV_ID", nullable = false)},
//	inverseJoinColumns = { @JoinColumn(name = "BRANCH_ID", nullable = false)
//		})
//	public Branch getBranch() {
//		return branch;
//	}
//
//	public void setBranch(Branch branch) {
//		this.branch = branch;
//	}

	
	
	 
	
}
