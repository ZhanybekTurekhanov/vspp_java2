package cz.vspp.javaproject.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.util.AutoPopulatingList;

import javax.persistence.CascadeType;

@Entity
@Table(name = "dev_team", catalog = "vspp")
public class DevTeam implements Serializable{

	private Integer teamId;
	private String teamName;
	private String teamCode;
	private String spec;
	private List<Developer> devs = new AutoPopulatingList<Developer>(Developer.class);
	
	public DevTeam() {
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TEAM_ID", nullable= false)
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	@Column(name = "TEAM_NAME", nullable = false)
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	@Column(name = "TEAM_CODE")
	public String getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}
	
	@Column(name="SPEC", nullable = false)
	public String getSpec() {
		return spec;
	}


	public void setSpec(String spec) {
		this.spec = spec;
	}


	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "developer_dev_team", joinColumns = { 
			@JoinColumn(name = "TEAM_ID", nullable = false)},
			inverseJoinColumns = { @JoinColumn(name = "DEV_ID", nullable = false)
	})
	public List<Developer> getDevs() {
		return devs;
	}

	public void setDevs(List<Developer> devs) {
		this.devs = devs;
	}
	
	
	
	
}
