package cz.vspp.javaproject.controllers;

import org.dom4j.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.service.DevTeamService;
import cz.vspp.javaproject.service.DeveloperService;

@Controller
public class DeveloperController {
	
	@Autowired
	private DeveloperService developerService;
	
	@Autowired
	private DevTeamService devTeamService;

	
//	@ModelAttribute("teams")
//    public List<DevTeam> getTeams(){
//        return devTeamService.listDevTeams();
//    }
//	
//	@ModelAttribute("branches")
//    public List<Branch> getBranches(){
//        return branchService.listBranch();
//    }

	@RequestMapping(value = "/createdev", method = RequestMethod.GET)
	public String createDev(Model model){
		
		model.addAttribute("developer", new Developer());
		model.addAttribute("listDevs", developerService.listDeveloper());

		return "createDev";
	}
	
	@RequestMapping(value= "/createdev/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("developer") Developer p, BindingResult result){		

		if(result.hasErrors()){
			System.out.println("Oh noes!");
			for(ObjectError err :result.getAllErrors()){
				System.out.println(err.getDefaultMessage());
			}
			return "redirect:/createdev";
			
		}
		
		
		if(p.getId() != null ){
			developerService.updateDeveloper(p);
			
		}else{
			developerService.addDeveloper(p);
			
		}
		
		return "redirect:/createdev";
		
	}
	
	@RequestMapping("/remove/{id}")
    public String removeDev(@PathVariable("id") int id){
		
        this.developerService.removeDeveloper(id);
        return "redirect:/createdev";
    }
 
    @RequestMapping("/edit/{id}")
    public String editDev(@PathVariable("id") int id, Model model){
        model.addAttribute("developer", this.developerService.getDeveloperById(id));
        model.addAttribute("listDevs", this.developerService.listDeveloper());
        return "createDev";
    }
    

    
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
    	binder.registerCustomEditor(Integer.class, "age", new CustomNumberEditor(Integer.class, false));

    }
	
}
