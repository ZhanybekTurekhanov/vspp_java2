package cz.vspp.javaproject.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.service.DevTeamService;
import cz.vspp.javaproject.service.DeveloperService;

@Controller
public class DevTeamController {

	@Autowired
	private DevTeamService devTeamService;
	
	@Autowired
	private DeveloperService developerService;
	
	@ModelAttribute("developers")
    public Set<Developer> getDevs(){
        return developerService.listDeveloper();
    }
	
	@RequestMapping(value = "/createteam", method = RequestMethod.GET)
	public String createDevTeam(Model model){
		
		
		model.addAttribute("devTeam", new DevTeam());
		model.addAttribute("listDevTeams", devTeamService.listDevTeams());

		return "createTeam";
	}
	
	@RequestMapping(value= "/createteam/add", method = RequestMethod.POST)
	public String addTeam(@ModelAttribute("devTeam") DevTeam team, BindingResult result, ModelMap model ){
		
		if(team.getTeamId() == null ){
			if(result.hasErrors()){
				System.out.println("Oh noes!");
				for(ObjectError err :result.getAllErrors()){
					System.out.println(err.getDefaultMessage());
				}
				
			}
			
			devTeamService.addDevTeam(team);
		}else{
			devTeamService.updateDevTeam(team);
		}
		
		return "redirect:/createteam";
		
	}
	
	@RequestMapping("/createteam/remove/{id}")
    public String removeDevTeam(@PathVariable("id") int id){
		
        this.devTeamService.removeDevTeam(id);
        return "redirect:/createteam";
    }
 
    @RequestMapping("/createteam/edit/{id}")
    public String editDevTeam(@PathVariable("id") int id, Model model){
    	DevTeam team = devTeamService.getDevTeamById(id);
//    	model.addAttribute("developers",team.getDevs());
        model.addAttribute("devTeam", this.devTeamService.getDevTeamById(id));
        model.addAttribute("listDevTeams", this.devTeamService.listDevTeams());
        return "createTeam";
    }
    
    @RequestMapping(value="/createteam/assigned/{id}", method = RequestMethod.GET)
    public String assignDevTeamPage(@PathVariable("id") int id, Model model){
    	DevTeam team = devTeamService.getDevTeamById(id);
        model.addAttribute("devTeam",team);
        model.addAttribute("listDevs", team.getDevs());
        return "assignTeam";
    }
    
//    @RequestMapping(value="/createteam/assigned/{id}", method = RequestMethod.POST)
//    public String assignDevTeam(@PathVariable("id") int id, Model model){
//    	List<Developer> assignedDevs = new ArrayList<>();
//    	
//    	assignedDevs.addAll( devTeamService.getDevTeamById(id).getDevs());
//    	
//        model.addAttribute("devTeam", devTeamService.getDevTeamById(id));
//        model.addAttribute("assignedDevs", assignedDevs);
//        model.addAttribute("listDevs", developerService.listDeveloper());
//        List<Integer> ids = new ArrayList<>();
//		for(Developer dev: developerService.listDeveloper()){
//			ids.add(dev.getId());
//		}
//		model.addAttribute("ids", ids);
//        return "assignTeam";
//    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
        binder.registerCustomEditor(List.class,"devs", new CustomCollectionEditor(List.class){
            protected Object convertElement(Object element){
                if (element instanceof String) {
                    Developer dev = developerService.getDeveloperById(Integer.parseInt(element.toString()));
                    return dev;
                }
                return null;
            }
        });
    }
}
