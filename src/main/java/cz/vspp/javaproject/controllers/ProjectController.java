package cz.vspp.javaproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.vspp.javaproject.editors.DevTeamEditor;
import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.entity.Project;
import cz.vspp.javaproject.service.DevTeamService;
import cz.vspp.javaproject.service.ProjectService;

@Controller
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private DevTeamService devTeamService;
	
	@RequestMapping(value = "/createproject", method = RequestMethod.GET)
	public String createProject(Model model){
		
		
		model.addAttribute("project", new Project());
		model.addAttribute("projects", projectService.listProject());
		model.addAttribute("listDevTeams", devTeamService.listDevTeams());

		return "createProject";
	}
	
	@RequestMapping(value= "/createproject/add", method = RequestMethod.POST)
	public String addProject(@ModelAttribute("project") Project project, BindingResult result){
		
		if(project.getTeamId() == null ){
			if(result.hasErrors()){
				for(ObjectError err :result.getAllErrors()){
					System.out.println(err.getDefaultMessage());
				}
				
			}

			projectService.addProject(project);
		}else{
			projectService.updateProject(project);
		}
		
		return "redirect:/createproject";
		
	}
	
	@RequestMapping("/createproject/remove/{id}")
    public String removeProject(@PathVariable("id") int id){
		
        projectService.removeProject(id);
        return "redirect:/createproject";
    }
 
    @RequestMapping("/createproject/edit/{id}")
    public String editProject(@PathVariable("id") int id, Model model){
        model.addAttribute("project", projectService.getProjectById(id));
        model.addAttribute("projects", projectService.listProject());
        model.addAttribute("listDevTeams", devTeamService.listDevTeams());
        return "createProject";
    }
    
    @RequestMapping(value="/createproject/project/{id}", method = RequestMethod.GET)
    public String assignedProject(@PathVariable("id") int id, Model model){
    	DevTeam team = devTeamService.getDevTeamById(id);
        model.addAttribute("project", projectService.getProjectById(id));
        model.addAttribute("devTeam", devTeamService.getDevTeamById(id));
        return "assignedProject";
    }
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) throws Exception{
    	
    	
        binder.registerCustomEditor(DevTeam.class,"devTeam", new DevTeamEditor(devTeamService));
    }

}
