package cz.vspp.javaproject.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.vspp.javaproject.entity.DevTeam;

public class DeveloperModel {
	
	private Integer id;
	private String name;
	private Integer age;
	private String position;
	private List<DevTeam> teams;
	
	public DeveloperModel() {
		super();
	}

	public DeveloperModel(Integer id, String name, Integer age, String position, List<DevTeam> teams) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.position = position;
		this.teams = teams;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public List<DevTeam> getTeams() {
		return teams;
	}

	public void setTeams(List<DevTeam> teams) {
		this.teams = teams;
	}
	
	

}
