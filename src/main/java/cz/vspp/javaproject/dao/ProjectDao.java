package cz.vspp.javaproject.dao;

import java.util.List;

import cz.vspp.javaproject.entity.Project;

public interface ProjectDao {
	
	public void addProject(Project p);
	public void updateProject(Project p);
	public List<Project> listProjects();
	public Project getProjectmById(int id);
	public void removeProject(int id);

}
