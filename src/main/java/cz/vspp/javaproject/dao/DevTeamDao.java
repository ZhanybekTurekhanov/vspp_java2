package cz.vspp.javaproject.dao;

import java.util.List;

import cz.vspp.javaproject.entity.DevTeam;


public interface DevTeamDao {
	
	public void addDevTeam(DevTeam p);
	public void updateDevTeam(DevTeam p);
	public List<DevTeam> listDevTeams();
	public DevTeam getDevTeamById(int id);
	public void removeDevTeam(int id);

}
