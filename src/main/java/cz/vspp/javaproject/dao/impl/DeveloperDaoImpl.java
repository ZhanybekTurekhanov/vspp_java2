package cz.vspp.javaproject.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.vspp.javaproject.dao.DeveloperDao;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.utils.HibernateUtil;

@Repository
public class DeveloperDaoImpl implements DeveloperDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addDeveloper(Developer p) {
		
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p);
        session.getTransaction().commit();  
        session.close();
		
	}

	@Override
	public void updateDeveloper(Developer p) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(p);
        session.getTransaction().commit();  
        session.close();
	}

	@Override
	public List<Developer> listDeveloper() {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Developer> devs = session.createQuery("SELECT d FROM Developer d").list();	
		session.close();
		return devs;
	}

	@Override
	public Developer getDeveloperById(int id) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Developer dev =(Developer) session.byId(Developer.class).load(id);
		session.close();
		return dev;
	}

	@Override
	public void removeDeveloper(int id) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Developer dev =(Developer) session.byId(Developer.class).load(id);
		session.delete(dev);
		session.getTransaction().commit();  
        session.close();
		
	}
	
	private Session getSession() {
	    Session sess = getSessionFactory().getCurrentSession();
	    if (sess == null) {
	      sess = getSessionFactory().openSession();
	    }
	    return sess;
	  }


	  private SessionFactory getSessionFactory() {
	    return sessionFactory;
	  }

}
