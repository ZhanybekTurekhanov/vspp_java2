package cz.vspp.javaproject.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.vspp.javaproject.dao.ProjectDao;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.entity.Project;
import cz.vspp.javaproject.utils.HibernateUtil;

@Repository
public class ProjectDaoImpl implements ProjectDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addProject(Project p) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p);
        session.getTransaction().commit();  
        session.close();
		
	}

	@Override
	public void updateProject(Project p) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(p);
        session.getTransaction().commit();  
        session.close();
		
	}

	@Override
	public List<Project> listProjects() {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Project> projects = session.createQuery("SELECT d FROM Project d").list();	
		session.close();
		return projects;
	}

	@Override
	public Project getProjectmById(int id) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Project project =(Project) session.byId(Project.class).load(id);
		session.close();
		return project;
	}

	@Override
	public void removeProject(int id) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Project project =(Project) session.byId(Project.class).load(id);
		session.delete(project);
		session.getTransaction().commit();  
        session.close();
		
	}

}
