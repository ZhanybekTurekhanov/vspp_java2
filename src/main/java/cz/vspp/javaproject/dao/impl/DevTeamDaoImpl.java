package cz.vspp.javaproject.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.vspp.javaproject.dao.DevTeamDao;
import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.utils.HibernateUtil;

@Repository
public class DevTeamDaoImpl implements DevTeamDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addDevTeam(DevTeam p) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p);
        session.getTransaction().commit();  
        session.close();
		
	}

	@Override
	public void updateDevTeam(DevTeam p) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(p);
        session.getTransaction().commit();  
        session.close();
		
	}

	@Override
	public List<DevTeam> listDevTeams() {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<DevTeam> devTeams = session.createQuery("SELECT d FROM DevTeam d").list();	
		session.close();
		return devTeams;
	}

	@Override
	public DevTeam getDevTeamById(int teamId) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		DevTeam devTeam =(DevTeam) session.byId(DevTeam.class).load(teamId);
		session.close();
		return devTeam;
	}

	@Override
	public void removeDevTeam(int id) {
		sessionFactory = HibernateUtil.getSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		DevTeam devTeam = (DevTeam) session.byId(DevTeam.class).load(id);
		session.delete(devTeam);
		session.getTransaction().commit();  
        session.close();
		
	}

}
