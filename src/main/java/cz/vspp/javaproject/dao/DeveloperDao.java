package cz.vspp.javaproject.dao;

import java.util.List;
import cz.vspp.javaproject.entity.Developer;

public interface DeveloperDao {
	
	public void addDeveloper(Developer p);
	public void updateDeveloper(Developer p);
	public List<Developer> listDeveloper();
	public Developer getDeveloperById(int id);
	public void removeDeveloper(int id);

}
