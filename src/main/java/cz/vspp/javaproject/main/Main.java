package cz.vspp.javaproject.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.utils.HibernateUtil;

public class Main {

	public static void main(String[] args) {

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();  
        Session session = sessionFactory.openSession();  
        session.beginTransaction();  
        
        Developer dev = new Developer();
        Developer dev1 = new Developer();
        dev.setName("Johnyy");
        dev.setPosition("Junior");
        dev.setAge(25);
        
        dev1.setName("Jackyy");
        dev1.setPosition("Senior");
        dev1.setAge(23);
        
        List<Developer> devs = new ArrayList<>();
        devs.add(dev);
        devs.add(dev1);
        
        DevTeam team1 = new DevTeam();
        //DevTeam team2 = new DevTeam("two", "games");
        
        session.save(team1);
        //session.save(team2);
        
//        Set<DevTeam> teams = new HashSet<DevTeam>(); 
//        teams.add(team1);
        //teams.add(team2);
        
        team1.setDevs(devs);
        
        session.save(team1);
        session.getTransaction().commit();
        
        session.close();

	}

}
