package cz.vspp.javaproject.service;

import java.util.Set;

import cz.vspp.javaproject.entity.Developer;

public interface DeveloperService {
	
	public void addDeveloper(Developer p);
	public void updateDeveloper(Developer p);
	public Set<Developer> listDeveloper();
	public Developer getDeveloperById(int id);
	public void removeDeveloper(int id);

}
