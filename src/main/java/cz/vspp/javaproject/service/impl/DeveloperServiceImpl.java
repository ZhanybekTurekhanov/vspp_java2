package cz.vspp.javaproject.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.vspp.javaproject.dao.DeveloperDao;
import cz.vspp.javaproject.entity.Developer;
import cz.vspp.javaproject.service.DeveloperService;


@Service
public class DeveloperServiceImpl implements DeveloperService{
	
	@Autowired
	private DeveloperDao developerDao;

	@Override
	public void addDeveloper(Developer p) {
		developerDao.addDeveloper(p);
		
	}

	@Override
	public void updateDeveloper(Developer p) {
		developerDao.updateDeveloper(p);
		
	}

	@Override
	public Set<Developer> listDeveloper() {
		Set<Developer> temp = new HashSet<>();
		temp.addAll(developerDao.listDeveloper());
		return temp;
	}

	@Override
	public Developer getDeveloperById(int id) {
		Developer dev = developerDao.getDeveloperById(id);
		
		return dev;
	}

	@Override
	public void removeDeveloper(int id) {
		developerDao.removeDeveloper(id);
		
	}

}
