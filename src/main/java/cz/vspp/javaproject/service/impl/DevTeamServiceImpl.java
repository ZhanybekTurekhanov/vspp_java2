package cz.vspp.javaproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.vspp.javaproject.dao.DevTeamDao;
import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.service.DevTeamService;

@Service
public class DevTeamServiceImpl implements DevTeamService{

	@Autowired
	private DevTeamDao devTeamDao;
	
	@Override
	public void addDevTeam(DevTeam p) {
		devTeamDao.addDevTeam(p);		
	}

	@Override
	public void updateDevTeam(DevTeam p) {
		devTeamDao.updateDevTeam(p);
		
	}

	@Override
	public List<DevTeam> listDevTeams() {
		List<DevTeam> devTeams = devTeamDao.listDevTeams();
		return devTeams;
	}

	@Override
	public DevTeam getDevTeamById(int id) {
		DevTeam team = devTeamDao.getDevTeamById(id);
		return team;
	}

	@Override
	public void removeDevTeam(int id) {
		devTeamDao.removeDevTeam(id);
		
	}

}
