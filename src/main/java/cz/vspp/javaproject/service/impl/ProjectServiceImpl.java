package cz.vspp.javaproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.vspp.javaproject.dao.ProjectDao;
import cz.vspp.javaproject.entity.Project;
import cz.vspp.javaproject.service.ProjectService;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService{
	
	@Autowired
	private ProjectDao projectDao;

	@Override
	public void addProject(Project p) {
		projectDao.addProject(p);
		
	}

	@Override
	public void updateProject(Project p) {
		projectDao.updateProject(p);
		
	}

	@Override
	public List<Project> listProject() {		
		return projectDao.listProjects();
	}

	@Override
	public Project getProjectById(int id) {
		return projectDao.getProjectmById(id);
	}

	@Override
	public void removeProject(int id) {
		projectDao.removeProject(id);
		
	}

}
