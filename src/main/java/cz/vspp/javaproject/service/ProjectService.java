package cz.vspp.javaproject.service;

import java.util.List;

import cz.vspp.javaproject.entity.Project;

public interface ProjectService {

	public void addProject(Project p);
	public void updateProject(Project p);
	public List<Project> listProject();
	public Project getProjectById(int id);
	public void removeProject(int id);
}
