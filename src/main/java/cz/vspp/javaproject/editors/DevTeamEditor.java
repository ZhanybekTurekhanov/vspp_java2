package cz.vspp.javaproject.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;

import cz.vspp.javaproject.entity.DevTeam;
import cz.vspp.javaproject.entity.Project;
import cz.vspp.javaproject.service.DevTeamService;
import cz.vspp.javaproject.service.ProjectService;

public class DevTeamEditor extends PropertyEditorSupport{
	
	DevTeamService devTeamService;

	public DevTeamEditor(DevTeamService devTeamService) {
		this.devTeamService = devTeamService;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		
		try{
			
			DevTeam team = devTeamService.getDevTeamById(Integer.parseInt(text));
			System.out.println(team.getTeamName());
			this.setValue(team);
		}catch  (NullPointerException e){

		}
	}
}
